#include <iostream> //Including Library 
#include <string>
using namespace std;

void Program1()
{
    for (int i = 0; i <= 20; i++) {
        cout << i << "  ";
        //do this while the condition is still true 
    }
}

void Program2()
{
    for (int count = 1; count <= 128; count *= 2) {
        cout << count << " ";
    }
}

void Program3()
{
    int sum = 0;
    int n;
    cout << "Enter a value for n: ";
    cin >> n; 
    for (int i = 1; i <= n; i++) {
        sum += i;
        cout << "Sum: " << sum << endl;

    }
}

void Program4()
{
    string text;
    char lookForLetter; 
    int letterCount = 0; 
    cout << "Enter a string:  ";
    cin.ignore(); 
    getline(cin, text);
    cout << "Enter a letter: ";
    cin >> lookForLetter;
    for (int i = 0; i < text.size(); i++) {
        cout << "Letter" << i << ":  " << text[i] << endl;
        if (tolower(text[i]) == tolower(lookForLetter))
            letterCount++;
    }
    cout << endl;
    cout << " There are " << letterCount << " " << lookForLetter << "(s)" <<  "in " << "\"" << text << "\"" << endl; 
}

int main()
{
    // Don't modify main 
    while ( true )
    {
        cout << "Run which program? (1-4): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
